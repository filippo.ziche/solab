#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define MPANIC(msg) { printf(msg); exit(0); }
#define FPANIC(msg, ...) { printf(msg, __VA_ARGS__); exit(0); }

#define PREAD  1
#define PWRITE 0

int main(int argc, char** argv) {

	if (argc != 2)
		FPANIC("Usage: %s times\n", argv[0]);

	int times = atoi(argv[1]);
	if (times <= 0)
		return EXIT_FAILURE;

	int pipe_parent2child[2];
	int pipe_child2parent[2];


	if(pipe(pipe_parent2child) == -1 || pipe(pipe_child2parent) == -1)
		MPANIC("Errore creazione pipe\n");

	char tmp_buffer[5];
	switch(fork()) {
		case -1: MPANIC("Errore fork"); 
			break;

		// Figlio
		case 0: {
			if (close(pipe_parent2child[PWRITE]) == -1 || close(pipe_child2parent[PREAD]) == -1)
				MPANIC("<Child> Errore setup delle pipes del figlio\n");

			const char* text = "pong";
			size_t size = strlen(text) + 1;
			for(int i = 0; i < times; i++) {
				if (read(pipe_parent2child[PREAD], tmp_buffer, 5) == -1)
					MPANIC("<Child> Errore lettura da pipe\n");

				printf("<Child> %d - %s\n", i + 1, tmp_buffer);

				if (write(pipe_child2parent[PWRITE], text, size) == -1)
					MPANIC("<Child> Errore scrittura sulla pipe\n");
			}

			if (close(pipe_parent2child[PREAD]) == -1 || close(pipe_child2parent[PWRITE]) == -1)
				MPANIC("<Child> Errore chiusura delle pipes del figlio\n");
	
			exit(0);
		} break;
	
		// Padre
		default: {
			if (close(pipe_parent2child[PREAD]) == -1 || close(pipe_child2parent[PWRITE]) == -1)
				MPANIC("<Parent> Errore setup delle pipes del padre\n");
			
			const char* text = "ping";
			size_t size = strlen(text) + 1;
			for(int i = 0; i < times; i++) {
				if (write(pipe_parent2child[PWRITE], text, size) == -1)
					MPANIC("<Child> Errore scrittura sulla pipe\n");
				
				printf("<Child> %d - %s\n", i + 1, tmp_buffer);

				if (read(pipe_child2parent[PREAD], tmp_buffer, 5) == -1)
					MPANIC("<Child> Errore lettura da pipe\n");
			}	
			
			if (close(pipe_parent2child[PWRITE]) == -1 || close(pipe_child2parent[PREAD]) == -1)
				MPANIC("<Parent> Errore chiusura delle pipes del padre\n");

		} break;
	}

	return EXIT_SUCCESS;
}
