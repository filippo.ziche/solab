#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#define PIPE_READ  0
#define PIPE_WRITE 1

#define MSG_BYTES 100

extern int errno;
inline void panic(const char* msg) {
	printf("[%d]: %s\n", errno, msg);
	exit(0);
}

void consumer(int pipefd[2]) {
	if (close(pipefd[PIPE_WRITE]) == -1)
		panic("<Consumer> Errore chiusura write end della pipe");

	// Legge contenuto del file
	ssize_t read_bytes = -1;
	char buffer[MSG_BYTES];
	do {
		read_bytes = read(pipefd[PIPE_READ], buffer, MSG_BYTES);
		printf("<Consumer> Letti %d bytes dalla pipe\n", read_bytes);
		
		if (read_bytes == -1) {
			printf("<Consumer> Errore lettura dalla pipe\n");
		} else if (read_bytes == 0) {
			printf("<Consumer> Non ci sono più write end aperti nella pipe\n");
		} else {
			buffer[read_bytes] = '\0';
			printf("<Consumer> line: %s\n", buffer);
		}
	} while(read_bytes > 0);	

	if (close(pipefd[PIPE_READ]) == -1)
		panic("<Consumer> Errore chiusura read end della pipe");
}

void producer(int pipefd[2], char* filename) {
	if (close(pipefd[PIPE_READ]) == -1)
		panic("<Producer> Errore chiusura read end della pipe");
	
	int file = open(filename, O_RDONLY);
	if (file == -1)
		panic("<Producer> Errore apertura file");

	char buffer[MSG_BYTES];
	ssize_t write_bytes = -1, read_bytes = 0;
	do {
		// Read from file
		read_bytes = read(file, buffer, MSG_BYTES);
		printf("<Producer> Letti %d bytes dal file\n", read_bytes);

		if (read_bytes > 0) {
			write_bytes = write(pipefd[PIPE_WRITE], buffer, read_bytes);
			printf("<Producer> Scritti %d bytes nella pipe\n", write_bytes);	

			if (write_bytes != read_bytes)
				panic("Errore scrittura nella pipe");
		}

	} while (read_bytes > 0);
	close(file);

	if (close(pipefd[PIPE_WRITE]) == -1)
		panic("<Producer> Errore chiusura write end della pipe");
}


int main(int argc, char** argv) {

	if (argc != 2) {
		printf("Usage: %s filename\n", argv[0]);
		return EXIT_FAILURE;	
	}

	int pipefd[2];
	if (pipe(pipefd) == -1) {
		printf("Impossibile creare pipe\n");
		return EXIT_FAILURE;
	}

	printf("<Consumer> creazione sottoprocesso\n");
	switch(fork()) {
		// Errore
		case -1: {
			printf("Errore creazione sottoprocesso\n");
			return EXIT_FAILURE;
		} break;


		// Child
		case 0: {
			producer(pipefd, argv[1]);
			exit(0);
		} break;

		// Parent
		default: {
			consumer(pipefd);
		}	
	}

	return EXIT_SUCCESS;

}
