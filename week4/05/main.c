#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>


void sig_handler(int sig) {
	printf("Che bella dormita!\n");
}

int main(int argc, char** argv) {
	if (argc != 2) {
   		printf("Usage: %s time\n", argv[0]);
		return EXIT_FAILURE;
	}

	int time = atoi(argv[1]);
	if (time <= 0) {
		return EXIT_FAILURE;
	}

	// Abilita solo il segnale di allarme
	sigset_t set;
	sigfillset(&set);
	sigdelset(&set, SIGALRM);
	sigprocmask(SIG_BLOCK, &set, NULL);

	// Setta signal handler
	if (signal(SIGALRM, sig_handler) == SIG_ERR) {
		printf("Errore sig handler\n");
		return EXIT_FAILURE;
	}

	alarm(time);
	pause();

	return EXIT_SUCCESS;	
}
