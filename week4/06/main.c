
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

pid_t child1, child2;

void handler_parent(int sig) {
	switch(sig) {
		case SIGUSR1: {
			printf("<Parent> Ricevuto SIGUSR1, Invio SIGUSR1 al secondo figlio\n");
  			kill(child2, SIGUSR1);			
		} break;
	
		case SIGUSR2: {
			printf("<Parent> Rievuto SIGUSR2, Invio SIGUSR2 al primo figlio\n ");
			kill(child1, SIGUSR2);
		} break;
	}
}

void handler_child1(int sig) {
	if (sig == SIGUSR2) {
		printf("<Child 1> Ricevuto SIGUSR2\n");
		exit(0);
	}
}

void handler_child2(int sig) {
	if (sig == SIGUSR1) {
		printf("<Child 2> Rivecuto SIGUSR1, Invio SIGUSR2 a padre\n");
		kill(getppid(), SIGUSR2);
		exit(0);
	}
}


int main(int argc, char** argv) {
	
	// Blocca tutti i segnali
	sigset_t set;
	sigfillset(&set);
	sigprocmask(SIG_BLOCK, &set, NULL);

	// Crea primo figlio, anche lui avrà tutti
	// i messaggi bloccati
	child1 = fork();
	if (child1 == -1) {
		printf("Errore creazione figlio 1\n");
		return EXIT_FAILURE;
	}

	// Figlio uno
	if (child1 == 0) {
		if (signal(SIGUSR2, handler_child1) == SIG_ERR) {
			printf("Errore settaggio handler figlio");
			exit(1);
		}	
	
		// Abilita SIGUSR2 per il figlio
		sigfillset(&set);
		sigdelset(&set, SIGUSR2);
		sigprocmask(SIG_SETMASK, &set, NULL);	

		printf("<Child 1> sta inviando SIGUSR1 al padre\n");
		kill(getppid(), SIGUSR1);

		// Aspetta SIGUSR2 dall'altro figlio
		pause();
		exit(0);
	}

	// Crea secondo figlio, anche lui avrà tutti
	// i messaggi bloccati
	child2 = fork();
	if (child2 == -1) {
		printf("Errore creazione figlio 2\n");
		return EXIT_FAILURE;
	}

	// Figlio due
	if (child2 == 0) {
		if (signal(SIGUSR1, handler_child2) == SIG_ERR) {
			printf("Errore settaggio handler figlio");
			exit(1);
		}	
	
		// Abilita SIGUSR1 per il figlio
		sigfillset(&set);
		sigdelset(&set, SIGUSR1);
		sigprocmask(SIG_SETMASK, &set, NULL);	

		// aspetta SIGUSR1
		pause();
		exit(0);
	}


	// Setta handlers per i due eventi
	if (signal(SIGUSR1, handler_parent) == SIG_ERR || signal(SIGUSR2, handler_parent) == SIG_ERR) {
		printf("Errore settaggio handlers del padre\n");
		return EXIT_FAILURE;
	}

	// Blocca tutti i messaggi tranne SIGUSR1 e SIGUSR2
	sigfillset(&set);
	sigdelset(&set, SIGUSR1);
	sigdelset(&set, SIGUSR2);
	sigprocmask(SIG_SETMASK, &set, NULL);

	// Attende i figli
	while(wait(NULL) != -1);
	printf("Entrambi i processi figli hanno terminato\n");

	return EXIT_SUCCESS;
}

