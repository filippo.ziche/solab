#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/wait.h>


#define PIPE_READ  0
#define PIPE_WRITE 1

#define MSG_BYTES 100

inline void panic(const char* msg) {
	printf("%s\n", msg);
	exit(0);
}

void consumer(int pipefd[2]) {
	if (close(pipefd[PIPE_WRITE]) == -1)
		panic("<Consumer  > Errore chiusura write end della pipe");

	// Legge contenuto del file
	ssize_t read_bytes = -1;
	char buffer[MSG_BYTES + 1];
	do {

		// Legge dimensione del messaggio
		ssize_t msg_size;
		read_bytes = read(pipefd[PIPE_READ], &msg_size, sizeof(ssize_t));
		if (read_bytes < sizeof(ssize_t))
			printf("<Consumer  > Sembra che non ci sia la dimensione del messaggio\n");
		else if (read_bytes == 0)
			printf("<Consumer  > Sembra che tutti gli ingressi in scrittura sulla pipe siano stati chiusi\n");
		else if (read_bytes > 0) {
			printf("<Consumer  > Ricevuto messaggio da %d bytes\n", msg_size);

			// Legge messaggio
			read_bytes = read(pipefd[PIPE_READ], buffer, msg_size);
			if (read_bytes == 0)
				printf("<Consumer  > Sembra che tutti gli ingressi in scrittura sulla pipe siano stati chiusi\n");
			else if (read_bytes < msg_size)
				printf("<Consumer  > Sembra che il messaggio non sia della dimensione giusta\n");
			else if (read_bytes > 0) {
				printf("<Consumer  > Letti %d bytes del messaggio\n", read_bytes);

				buffer[read_bytes] = '\0';
				printf("<Consumer  > messaggio: %s\n", buffer);
			}
			else
				printf("<Consumer  > Sembra che la pipe sia rotta\n");
		}
		else
			printf("<Consumer  > Sembra che la pipe sia rotta\n");

	} while(read_bytes > 0);	

	if (close(pipefd[PIPE_READ]) == -1)
		panic("<Consumer  > Errore chiusura read end della pipe");
}

typedef struct chunk_t {
	ssize_t size;
	char buffer[MSG_BYTES];
} chunk_t;

void producer(int i, int pipefd[2], char* filename) {
	if (close(pipefd[PIPE_READ]) == -1)
		panic("<Producer> Errore chiusura read end della pipe");
	
	int file = open(filename, O_RDONLY);
	if (file == -1)
		panic("<Producer> Errore apertura del file");
	
	chunk_t chunk = { 0 };
	ssize_t write_bytes = -1;
	do {
		// Read from file
		chunk.size = read(file, chunk.buffer, MSG_BYTES);
		if (chunk.size > 0) {
			printf("<Producer %d> Letti %d bytes del file\n", i, chunk.size);

			write_bytes = write(pipefd[PIPE_WRITE], &chunk, chunk.size + sizeof(ssize_t));
			if (write_bytes == -1)
				panic("<Producer> Errore scrittura nella pipe");
			printf("<Producer %d> Scritti %d (%d + 8) bytes nella pipe\n", i, write_bytes, chunk.size);
		}
		else if (chunk.size == -1)
			panic("<Producer> Errore lettura dal file");
		
	} while (chunk.size > 0);
	close(file);

	if (close(pipefd[PIPE_WRITE]) == -1)
		panic("<Producer> Errore chiusura write end della pipe");
}


int main(int argc, char** argv) {

	if (argc < 2) {
		printf("Usage: %s filename1 ... filenameN \n", argv[0]);
		return EXIT_FAILURE;	
	}

	int pipefd[2];
	if (pipe(pipefd) == -1) {
		printf("Impossibile creare pipe\n");
		return EXIT_FAILURE;
	}

	int count = argc - 1;
	printf("<Consumer  > creazione %d sottoprocessi\n", count);
	for (int i = 0; i < count; i++) {
		pid_t pid = fork();
		if (pid == -1)
			panic("Errore creazione sottoprocesso");
		else if (pid == 0) {
			producer(i, pipefd, argv[i + 1]);
			exit(0);
		}
	}

	consumer(pipefd);

	// Attende la fine di tutti i figli processo
	while (wait(NULL) != -1);
	return EXIT_SUCCESS;
}
