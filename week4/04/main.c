#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>

#define PANIC(msg) { printf(msg); exit(0); }

void callback(int sig) {
	printf("ctrl-c non mi può fermare!\n");
}

int main(int argc, char** argv) {

	// Blocca tutti i segnali tranne SIGINT
	sigset_t sigset;
	sigfillset(&sigset);
	sigdelset(&sigset, SIGINT);
	sigprocmask(SIG_BLOCK, &sigset, NULL);

	if (signal(SIGINT, callback) == SIG_ERR)
		PANIC("Errore settaggio callback\n");

	while(1) {
		printf("Ciao, io sono un processo in esecuzione (%d)\n", getpid());

		unsigned int t = 5; // secondi
		while (t != 0)
			t = sleep(t);
	}

	return EXIT_SUCCESS;
}
