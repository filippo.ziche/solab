#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#define BUFFER_SZ 150

extern char** environ;

/**
 * Check if base is subdir of test, returns -1 if it is not,
 * 0 if they are the same path, 1 if it is a subdir
 */
int issubdir(char* base, char* test) {
	
	size_t base_len = strlen(base); 
	size_t test_len = strlen(test);

	if (base_len > test_len)
		return -1;

	int c = 0;
	while (c != base_len && test[c] != '\0') {
		if (base[c] != test[c])
			return -1;
		c += 1;
	}

	// /tmp/test e /tmp/test2 non vanno bene
	if (test[c + 1] != '\\')
		return -1;

	return (base_len == test_len) ? 0 : 1;	
}

int main(int argc, char** argv) {

	char* username = getenv("USER");
	if (username == NULL) {
		username = "unknown";
	}

	char* home = getenv("HOME");
	if (home == NULL) {
		printf("Home non settata!");
		exit(0);
	}

	char working_dir[BUFFER_SZ];
	char* res = getcwd(working_dir, BUFFER_SZ);
	if (res == NULL) {
		printf("CWD non settata!");
		exit(0);
	}

	int subdir = issubdir(home, working_dir);

	if (subdir == 0) {
		printf("Caro %s, sono già al posto giusto!\n", username);			
	}
	else {

		chdir(home);
		int fd = open("empty_file.txt", O_RDWR | O_CREAT | O_TRUNC,
				S_IRUSR, S_IWUSR);
		
		if (fd == -1) {
			printf("Impossibile aprire file!");
			exit(0);
		}

		close(fd);
		printf("Case %s, sono dentro la tua home!\n", username);
	}

	return EXIT_SUCCESS;
}


