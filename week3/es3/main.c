#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char** argv) {
	
	if (argc != 2) {
		printf("Wrong usage!\n");
		return EXIT_FAILURE;
	}

	int proc_count = atoi(argv[1]);
	if (proc_count < 0) {
		printf("Invalid argument: N\n");
		return EXIT_FAILURE;
	} 

	srand(time(0));	

	pid_t pid;
	for (int i = 0; i < proc_count; i++) {
		int code = rand() % 256;		

		pid = fork();
		if (pid == -1) /* ERROR */ {
			printf("Errore creazione figlio\n");
			return EXIT_FAILURE;
		}
		else if (pid == 0) /* CHILD */ {
			printf("PID: %d, PPID: %d, Exit code: %d\n", getpid(), getppid(), code);
			return code;
		}
	}

	int res;
	int status;
	do {
		res = waitpid(pid, &status, WNOHANG);
		if (res == -1) {
			printf("Errore durante l'attesa\n");
			return EXIT_FAILURE;	
		}
	} while (res == 0);

	printf("Last child, PID: %d => %d\n", pid, WEXITSTATUS(status));
	return EXIT_SUCCESS;
}
