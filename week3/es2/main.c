#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char** argv) {
	
	if (argc != 2) {
		printf("Wrong usage!\n");
		return EXIT_FAILURE;
	}

	int proc_count = atoi(argv[1]);
	if (proc_count < 0) {
		printf("Invalid argument: N\n");
		return EXIT_FAILURE;
	} 

	srand(time(0));	

	pid_t pid;
	for (int i = 0; i < proc_count; i++) {
		int code = rand() % 256;		

		pid = fork();
		if (pid == -1) /* ERROR */ {
			printf("Errore creazione figlio\n");
			return EXIT_FAILURE;
		}
		else if (pid == 0) /* CHILD */ {
			printf("PID: %d, PPID: %d, Exit code: %d\n", getpid(), getppid(), code);
			return code;
		}
	}

	int child_status;
	pid_t child_pid;
	while ((child_pid = wait(&child_status)) != -1) {
		printf("CPID: %d => %d\n", child_pid, WEXITSTATUS(child_status));
	}
	
	/*
	if (errno != ECHILD) {
		printf("Errore sconosciuto!\n");
		return EXIT_FAILURE;
	} 
	*/

	return EXIT_SUCCESS;
}
