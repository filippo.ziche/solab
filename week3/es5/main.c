#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

extern int errno;

int main(int argc, char** argv) {
	
	if (argc < 2) {
		printf("Usage: %s cmd [args] \n", argv[0]);
		return EXIT_FAILURE;
	}

	pid_t pid = fork();
	if (pid == -1) /* ERRORE */ {

	} else if (pid == 0) /* FIGLIO */ {
		
		// Chiude standard output e error
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		// Crea nuovo file sostitutivo allo standard output ed error
		int log_fd = open("output", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
		if (log_fd == -1) {
			printf("Errore creazione file di logging\n");
			return EXIT_FAILURE;
		}

		// Fai puntare il fd dell'error stream al nuovo file
		dup(log_fd);

		execvp(argv[1], argv + 1);
		printf("Errore exec!\n");
	}

	/* PADRE */

	// Attende l'unico figlio
	int status;
	int res = wait(&status);
	if (res == -1 && errno != ECHILD) {
		printf("Errore sconosciuto\n");
		return EXIT_FAILURE;	
	}

	printf("Comando %s terminato con status %d\n", argv[1], WEXITSTATUS(status));
	return EXIT_SUCCESS;
}
