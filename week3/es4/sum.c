#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv) {
	if (argc != 3) {
		printf("Wrong usage!\n");
		return EXIT_FAILURE;
	}

	int n = atoi(argv[1]);
	int m = atoi(argv[2]);

	printf("Il prodotto di n e m è: %d\n", n * m);
	return EXIT_SUCCESS;
}
