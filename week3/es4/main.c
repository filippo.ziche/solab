#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <unistd.h>

#define MAX_VAL 10000
#define STR_LEN 5

#define PATH_LEN 64

int main(int argc, char** argv) {

	srand(time(0));
	int n = rand() % (MAX_VAL + 1);
	int m = rand() % (MAX_VAL + 1);

	char n_str[STR_LEN];
	snprintf(n_str, STR_LEN, "%d", n);

	char m_str[STR_LEN];
	snprintf(m_str, STR_LEN, "%d", m);

	// VISTO CHE DOBBIAMO USARE IL PERCORSO ASSOLUTO E NON RELATIVO...

	char cwd[PATH_LEN];
	char* res = getcwd(cwd, PATH_LEN);
	if (res == NULL) {
		printf("Errore\n");
		return EXIT_FAILURE;	
	}	

	char exe_loc[PATH_LEN];
	snprintf(exe_loc, PATH_LEN, "%s/sum", cwd);

	execlp(exe_loc, "sum", n_str, m_str, (char*)NULL);
}

