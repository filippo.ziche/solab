#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <signal.h>

#define PANIC(expr, msg) { if(expr) { printf(msg); exit(1); } }

// Coda messaggi
int queue = -1;

struct order_t {
	int mtype;

	unsigned int code;
	char description[100];
	unsigned int quantity;
	char email[100];	
};

void print_order(struct order_t* order) {
	printf("=======================================\n");	
	printf("Order:\n");
	printf("\tcode: %d\n", order->code);	
	printf("\tdesc: %s\n", order->description);	
	printf("\tquantity: %d\n", order->quantity);
	printf("\temail: %s\n", order->email);	
	printf("=======================================\n");	
}

void sig_handler(int sig) {
	if(sig == SIGINT && queue > 0) {
		msgctl(queue, IPC_RMID, NULL);		
	}

	exit(0);
}

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: %s <message queue key>", argv[0]);
		return EXIT_FAILURE;
	}

	// Legge la key della coda specificta dall'utente
	int key = atoi(argv[1]);
	PANIC(key <= 0, "<Server> La key deve essere maggiore di zero\n");

	// Blocca tutti gli eventi tranne SIGTERM	
	sigset_t set;
	sigfillset(&set);
	sigdelset(&set, SIGTERM);
	sigprocmask(SIG_BLOCK, &set, NULL);

	PANIC(signal(SIGTERM, &sig_handler) == SIG_ERR, "<Server> Errore creazione sig. handler\n");

	printf("<Server> Creazione coda messaggi...\n");
	queue = msgget(key, IPC_CREAT | S_IRUSR | S_IWUSR);
	PANIC(queue == -1, "<Server> Errore creazione coda\n");

	struct order_t order;
	printf("<Server> Lettura messaggi...\n");
	while(1) {
		size_t size = sizeof(struct order_t) - sizeof(int);
		int res = msgrcv(queue, &order, size, 0, 0);
		PANIC(res == -1, "<Server> Errore lettura coda\n");
		print_order(&order);
	}
}
