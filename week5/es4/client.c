#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/ipc.h>

struct order_t {
	int mtype;

	unsigned int code;
	char description[100];
	unsigned int quantity;
	char email[100];	
};

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: %s <message queue key>\n", argv[0]);
		exit(1);	
	}

	int key = atoi(argv[1]);
	if (key <= 0) {
		printf("<Client> Key deve essere maggiore di zero\n");
		exit(1);
	}	

	int queue = msgget(key, IPC_CREAT | S_IWUSR | S_IRUSR);
	if (queue == -1) {
		printf("<Client> Errore apertura coda\n");
		exit(1);
	}

	struct order_t order = { 0 };
	
	printf("<Client> Inserisci code: ");
	scanf("%d", &order.code);

	printf("<Client> Inserisci descrizione: ");
	//scanf("%s\n", order.description);
	sprintf(order.description, "Ciao");

	printf("<Client> Inserisci quantità: ");
	//scanf("%d", &order.quantity);

	printf("<Client> Inserisci email: ");
	//scanf("%s\n", order.email);
	sprintf(order.email, "Mi chiamo giorgia");

	printf("<Client> Invio del messaggio...");
	size_t size = sizeof(struct order_t) - sizeof(int);
	msgsnd(queue, &order, size, 0);
	printf("Done\n");

	return EXIT_SUCCESS;
}
