
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define FIFO_SERVER_PATH "/tmp/fifo_server"
#define FIFO_BASE_PATH "/tmp/fifo_client"

struct Request {
	pid_t pid;
	int code;
};

int main(int argc, char** argv) {
	
	char path2fifo[25];
	sprintf(path2fifo, "%s.%d", FIFO_BASE_PATH, getpid());

	printf("<Client> Creazione FIFO %s...\n", path2fifo);
	if (mkfifo(path2fifo, S_IWUSR | S_IRUSR | S_IWGRP) == -1) {
		printf("<Client> Errore creazione FIFI\n");
		exit(1);
	}
	printf("<Client> FIFO creata\n");

	printf("<Client> Apertura FIFO server\n");
	int fifofd_server = open(FIFO_SERVER_PATH, O_WRONLY);
	if (fifofd_server == -1) {
		printf("<Client> Errore apertura FIFO server\n");
		exit(1);
	}

	struct Request req = { getpid() };		
	printf("<Client> Invio dati\n");
	int res = write(fifofd_server, &req, sizeof(struct Request));
	if (res == -1) {
		printf("<Client> Errore scrittura nella FIFO\n");
		return EXIT_FAILURE;
	}

	printf("<Client> Apertura FIFO risposta\n");
	int fifofd = open(path2fifo, O_RDONLY);
	if (fifofd == -1) {
		printf("<Client> FIFO non presente\n");
		return EXIT_FAILURE;
	}

	int response;
	if (read(fifofd, &response, sizeof(int)) == -1) {
		printf("<Client> Errore ottenimento risposta\n");
		exit(1);
	}
	printf("<Client> Risposta: %d\n", response);


	close(fifofd);
	unlink(path2fifo);

	return EXIT_SUCCESS;
}	
