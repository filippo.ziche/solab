
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#define SERVER_FIFO_PATH "/tmp/fifo_server"
#define CLIENT_BASE_PATH "/tmp/fifo_client"

int fifofd, fifofd_extra;

// Handles quit by removing the fifo
void quit(int sig) {
	if (sig == SIGALRM)
		printf("<Server> Troppo tempo dall'ultima connessione\n");

	if (close(fifofd) == -1) {
		printf("<Server> Errore chiusura FIFO\n");
		exit(1);
	}

	if (fifofd_extra != 0 && close(fifofd_extra) == -1) {
		printf("<Server> Errore chiusura\n");
		exit(1);
	}

	unlink(SERVER_FIFO_PATH);
	exit(0);
}

struct Request {
	pid_t pid;
    int code;
};

void send_response(struct Request* req) {
	
	char path2clientFIFO[25];
	sprintf(path2clientFIFO, "%s.%d", CLIENT_BASE_PATH, req->pid);
	
	printf("<Server> apertura FIFO %s...\n", path2clientFIFO);
	int clientfifo = open(path2clientFIFO, O_WRONLY);
	if (clientfifo == -1) {
		printf("<Server> Errore apertura FIFO\n");
		exit(1);
	}

	printf("<Server> Invio risposta\n");
	int response = req->code * req->code;
	int bytes = write(clientfifo, &response, sizeof(int));
	if (bytes == -1) {
		printf("<Server> Errore scrittura risposta\n");
		exit(1);
	}

	close(clientfifo);
}

int main(int argc, char** argv) {

	printf("<Server> Making FIFO...\n");
	if (mkfifo(SERVER_FIFO_PATH, S_IWUSR | S_IRUSR | S_IWGRP) == -1) {
		printf("<Server> Errore creazione FIFO\n");
		return EXIT_FAILURE;
	}
	printf("<Server> FIFO %s created!\n", SERVER_FIFO_PATH);

	if (signal(SIGALRM, &quit) == SIG_ERR || signal(SIGINT, &quit) == SIG_ERR) {
		printf("<Server> Errore creazione signal handlers\n");
		return EXIT_FAILURE;
	}

	// Prima attesa
	alarm(30);

	printf("<Server> Waiting for client...\n");
	fifofd = open(SERVER_FIFO_PATH, O_RDONLY);
	
	// Evita di ricevere un EOF quanto tutti hanno smesso di comunicare
	fifofd_extra = open(SERVER_FIFO_PATH, O_WRONLY);
	if (fifofd_extra == -1) {
		printf("<Server> Apertura fallita\n");
		return EXIT_FAILURE;
	}

	struct Request req = { 0 };
	int res = -1;
	do {	
		// Resetta timer
		alarm(0);

		res = read(fifofd, &req, sizeof(struct Request));
		if (res == -1)
			printf("<Server> La FIFO sembra rotta\n");
		else if (res != sizeof(struct Request))
			printf("<Server> Non sono stati ricevuti due interi\n");
		else
			send_response(&req);

		alarm(30);
	} while(res != -1);

	return EXIT_SUCCESS;
}
