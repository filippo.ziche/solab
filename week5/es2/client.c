
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: %s filename\n", argv[0]);
		return EXIT_FAILURE;
	}

	char* path2fifo = argv[1];
	int fifofd = open(path2fifo, O_WRONLY);
	if (fifofd == -1) {
		printf("<Client> FIFO non presente\n");
		return EXIT_FAILURE;
	}

	int v[2] = { 0, 0 };
	printf("<Client> Inserire i due numeri: ");
	scanf("%d %d", v, v + 1);

	printf("<Client> Invio dati (%d, %d)\n", v[0], v[1]);
	int res = write(fifofd, v, sizeof(int) * 2);
	if (res == -1) {
		printf("<Client> Errore scrittura nella FIFO\n");
		return EXIT_FAILURE;
	}

	close(fifofd);
	return EXIT_SUCCESS;
}	
