
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

char* path2fifo;
int fifofd, fifofd_extra;

// Handles quit by removing the fifo
void quit(int sig) {
	if (sig == SIGALRM)
		printf("<Server> Troppo tempo dall'ultima connessione\n");

	if (close(fifofd) == -1) {
		printf("<Server> Errore chiusura FIFO\n");
		exit(1);
	}

	if (fifofd_extra != 0 && close(fifofd_extra) == -1) {
		printf("<Server> Errore chiusura\n");
		exit(1);
	}

	unlink(path2fifo);
	exit(0);
}

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: %s pathname\n", argv[0]);
		return EXIT_FAILURE;
	}

	path2fifo = argv[1];
	printf("<Server> Making FIFO...\n");
	if (mkfifo(path2fifo, S_IWUSR | S_IRUSR | S_IWGRP) == -1) {
		printf("<Server> Errore creazione FIFO\n");
		return EXIT_FAILURE;
	}

	printf("<Server> FIFO %s created!\n", path2fifo);
	if (signal(SIGALRM, &quit) == SIG_ERR) {
		printf("<Server> Errore creazione signal handler\n");
		return EXIT_FAILURE;
	}

	// Prima attesa
	alarm(30);

	printf("<Server> Waiting for client...\n");
	fifofd = open(path2fifo, O_RDONLY);

	
	fifofd_extra = open(path2fifo, O_WRONLY);
	if (fifofd_extra == -1) {
		printf("<Server> Apertura fallita\n");
		return EXIT_FAILURE;
	}

	int v[2] = { 0, 0 };
	int res = -1;
	do {	
		// Resetta timer
		alarm(0);

		res = read(fifofd, v, sizeof(int) * 2);
		if (res == -1)
			printf("<Server> La FIFO sembra rotta\n");
		else if (res != sizeof(int) * 2)
			printf("<Server> Non sono stati ricevuti due interi\n");
		else
			printf("<Server> %d is %s %d\n", v[0],
			   (v[0] > v[1]) ? "più grande di" : "più piccolo di",
			    v[1]);
		
		alarm(30);

	} while(v[0] != v[1] && res != -1);

	return EXIT_SUCCESS;
}
