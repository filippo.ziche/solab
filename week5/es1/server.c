
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: %s pathname\n", argv[0]);
		return EXIT_FAILURE;
	}

	char* path2fifo = argv[1];
	printf("<Server> Making FIFO...\n");
	if (mkfifo(path2fifo, S_IWUSR | S_IRUSR | S_IWGRP) == -1) {
		printf("<Server> Errore creazione FIFO\n");
		return EXIT_FAILURE;
	}

	printf("<Server> FIFO %s created!\n", path2fifo);
	printf("<Server> Waiting for client...\n");
	int fifofd = open(path2fifo, O_RDONLY);

	int v[2] = { 0, 0 };
	int res = read(fifofd, v, sizeof(int) * 2);

	if (res == -1)
		printf("<Server> La FIFO sembra rotta\n");
	else if (res != sizeof(int) * 2)
		printf("<Server> Non sono stati ricevuti due interi\n");
	else {
		printf("<Server> %d is %s %d\n", v[0],
			   (v[0] > v[1]) ? "più grande di" : "più piccolo di",
			    v[1]);
	}

	printf("<Server> Rimozione FIFO...\n");
	close(fifofd);
	unlink(path2fifo);
		
	return EXIT_SUCCESS;
}
